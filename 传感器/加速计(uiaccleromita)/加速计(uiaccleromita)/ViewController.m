//
//  ViewController.m
//  加速计(uiaccleromita)
//
//  Created by CavanSu on 17/3/2.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIAccelerometerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //1.获取单例对象
    UIAccelerometer *acclerometer = [UIAccelerometer sharedAccelerometer];
    
    //2.设置代理
    acclerometer.delegate=self;
    
    //3.设置代理间隔
    [acclerometer setUpdateInterval:1.0];
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    NSLog(@"x:%f,y:%f,z:%f",acceleration.x,acceleration.y,acceleration.z);
}

@end
