//
//  ViewController.m
//  距离传感器
//
//  Created by CavanSu on 17/3/2.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //打开距离传感器
    [UIDevice currentDevice].proximityMonitoringEnabled = YES;
    
    //监听物品靠近or离开
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityStateChange) name:UIDeviceProximityStateDidChangeNotification object:nil];
}

- (void)proximityStateChange {
    //有物品靠近
    if ([UIDevice currentDevice].proximityState) {
        NSLog(@"有物品靠近");
    }
    //有物品离开
    else {
        NSLog(@"有物品离开");
    }
}

@end
