//
//  AppDelegate.h
//  加速计(coremotion)
//
//  Created by CavanSu on 17/3/2.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

