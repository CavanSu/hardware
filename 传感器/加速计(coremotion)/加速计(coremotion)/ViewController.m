//
//  ViewController.m
//  加速计(coremotion)
//
//  Created by CavanSu on 17/3/2.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import <CoreMotion/CoreMotion.h>

@interface ViewController ()
@property (nonatomic, strong) CMMotionManager *motionManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // 加速计pull方式
//    CMAcceleration acceleration=self.motionManager.accelerometerData.acceleration;
//    NSLog(@"x:%f,y:%f,z:%f",acceleration.x,acceleration.y,acceleration.z);
    
    // 陀螺仪pull方式
    CMRotationRate rotationRate = self.motionManager.gyroData.rotationRate;
    NSLog(@"x: %f,y: %f,z: %f", rotationRate.x, rotationRate.y, rotationRate.z);
}

#pragma mark- 陀螺仪
#pragma mark- pull方式
- (void)pullGyro {
    //1.创建运动管理对象
    self.motionManager = [[CMMotionManager alloc] init];
    
    //2.判断陀螺仪是否可用
    if (!self.motionManager.isGyroAvailable) {
        NSLog(@"不可用");
        return;
    }
    
    //3.开始采集
    [self.motionManager startGyroUpdates];
}

#pragma mark- push方式
- (void)pushGyro {
    //1.创建运动管理对象
    self.motionManager = [[CMMotionManager alloc] init];
    
    //2.判断陀螺仪是否可用
    if (!self.motionManager.isGyroAvailable) {
        NSLog(@"不可用");
        return;
    }
    
    //4.设置采集间隔
    self.motionManager.magnetometerUpdateInterval = 1.0;
    
    //3.开始采集
    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMGyroData * _Nullable gyroData, NSError * _Nullable error) {
        CMRotationRate rotationRate = gyroData.rotationRate;
        NSLog(@"x: %f, y: %f, z: %f", rotationRate.x, rotationRate.y, rotationRate.z);
    }];
}

#pragma mark- 加速计
#pragma mark- pull方式
- (void)pullAccelerometer {
    //pull
    //1.创建运动管理对象
    self.motionManager = [[CMMotionManager alloc] init];
    
    //2.判断加速计是否可用
    if (!self.motionManager.isAccelerometerAvailable) {
        NSLog(@"不可用");
        return;
    }
    //3.获取信息
    [self.motionManager startAccelerometerUpdates];
}

#pragma mark- push方式
- (void)pushAccelerometer {
    //push
    //1.创建运动管理对象
    self.motionManager = [[CMMotionManager alloc] init];
    
    //2.判断加速计是否可用
    if (!self.motionManager.isAccelerometerAvailable) {
        NSLog(@"不可用");
        return;
    }
    
    //3.设置间隔
    self.motionManager.magnetometerUpdateInterval=1.0;
    
    //4.开始采样
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue]
                                             withHandler:^(CMAccelerometerData * _Nullable accelerometerData, NSError * _Nullable error) {
        //当采样到加速计信息执行block
        if (error) {
            return;
        }
        CMAcceleration acceleration=accelerometerData.acceleration;
        NSLog(@"x: %f, y: %f, z: %f", acceleration.x, acceleration.y, acceleration.z);
    }];
}

@end
