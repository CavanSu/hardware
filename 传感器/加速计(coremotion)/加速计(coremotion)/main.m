//
//  main.m
//  加速计(coremotion)
//
//  Created by CavanSu on 17/3/2.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
