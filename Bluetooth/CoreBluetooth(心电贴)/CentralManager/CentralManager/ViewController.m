//
//  ViewController.m
//  CentralManager
//
//  Created by CavanSu on 17/3/13.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#include <stdlib.h>
#include <stdio.h>

@interface ViewController () <CBCentralManagerDelegate, CBPeripheralDelegate>
/** 中心管理者 */
@property (nonatomic, strong) CBCentralManager *centralManager;
/** 连接到的外设 */
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) NSMutableArray *uuidArr;

@property (weak, nonatomic) IBOutlet UILabel *heartRate;
@property (weak, nonatomic) IBOutlet UILabel *ecg1;
@property (weak, nonatomic) IBOutlet UILabel *ecg2;
@property (weak, nonatomic) IBOutlet UILabel *ecg3;
@property (weak, nonatomic) IBOutlet UILabel *ecg4;

@property (weak, nonatomic) IBOutlet UILabel *acc1;
@property (weak, nonatomic) IBOutlet UILabel *acc2;
@property (weak, nonatomic) IBOutlet UILabel *acc3;
@property (weak, nonatomic) IBOutlet UILabel *acc4;
@property (weak, nonatomic) IBOutlet UILabel *acc5;

@property (nonatomic, strong) NSMutableArray *storeArr11;
@property (nonatomic, strong) NSMutableArray *storeArr12;
@property (nonatomic, strong) NSMutableArray *storeArr37;

@property (nonatomic, assign) NSInteger count11;
@property (nonatomic, assign) NSInteger count12;
@property (nonatomic, assign) NSInteger count37;
@end

@implementation ViewController

- (CBCentralManager *)centralManager {
    if (!_centralManager) {
        /*
         设置主设备的代理,CBCentralManagerDelegate
         必须实现的：
         - (void)centralManagerDidUpdateState:(CBCentralManager *)central;//主设备状态改变调用，在初始化CBCentralManager的时候 会打开设备，只有当设备正确打开后才能使用
         其他选择实现的代理中比较重要的：
         - (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI; //找到外设
         - (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral;//连接外设成功
         - (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;//外设连接失败
         - (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;//断开外设
         */
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                               queue:dispatch_get_main_queue() options:nil];
    }
    return _centralManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor orangeColor];
   
    self.count11 = 0;
    self.count12 = 0;
    self.count37 = 0;
    
    self.storeArr11 = [NSMutableArray array];
    self.storeArr12 = [NSMutableArray array];
    self.storeArr37 = [NSMutableArray array];
    
    self.uuidArr=[NSMutableArray array];
    
    // 调用get方法,先将中心管理者初始化
    [self centralManager];
    
#warning can only accept commands while in the powered on state不能在state非ON的情况下对我们的中心管理者进行操作
    // 此处不能搜索外设
    //[self.cMgr scanForPeripheralsWithServices:nil options:nil];
}

#pragma mark- <1. 只要中心管理者初始化,就会触发此代理方法>
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    /*
     CBCentralManagerStateUnknown = CBManagerStateUnknown,
     CBCentralManagerStateResetting = CBManagerStateResetting,
     CBCentralManagerStateUnsupported = CBManagerStateUnsupported,
     CBCentralManagerStateUnauthorized = CBManagerStateUnauthorized,
     CBCentralManagerStatePoweredOff = CBManagerStatePoweredOff,
     CBCentralManagerStatePoweredOn = CBManagerStatePoweredOn,
     */
    switch (central.state) {
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            // 在中心管理者成功开启后再进行一些操作
            // 搜索外设
            [self.centralManager scanForPeripheralsWithServices:nil // 通过某些服务筛选外设
                                              options:nil]; // dict,条件
            // 搜索成功之后,会调用我们找到外设的代理方法
            // - (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI; //找到外设
        }
            break;
            
        default:
            break;
    }
}

#pragma mark- <2. 发现外设后调用的方法>
- (void)centralManager:(CBCentralManager *)central // 中心管理者
 didDiscoverPeripheral:(CBPeripheral *)peripheral // 外设
     advertisementData:(NSDictionary *)advertisementData // 外设携带的数据
                  RSSI:(NSNumber *)RSSI { // 外设发出的蓝牙信号强度

//    NSLog(@"%s, line = %d, cetral = %@,peripheral = %@, advertisementData = %@, RSSI = %@", __FUNCTION__, __LINE__, central, peripheral, advertisementData, RSSI);
    
    
    // 需要对连接到的外设进行过滤
    // 1.信号强度(40以上才连接, 80以上连接)
    // 2.通过设备名(设备字符串前缀是 OBand)
    // 在此时我们的过滤规则是:有OBand前缀并且信号强度大于35
    // 通过打印,我们知道RSSI一般是带-的
    
    if ([peripheral.name hasPrefix:@"laSports"] && (ABS(RSSI.integerValue) > 35)) {
        // 在此处对我们的 advertisementData(外设携带的广播数据) 进行一些处理
        // 通常通过过滤,我们会得到一些外设,然后将外设储存到我们的可变数组中,
        // 标记我们的外设,让他的生命周期 = vc
        
        self.peripheral = peripheral;
        // 发现完之后就是进行连接
        [self.centralManager connectPeripheral:self.peripheral options:nil];
//        NSLog(@"%s, line = %d", __FUNCTION__, __LINE__);
    }
}

#pragma mark- <3.1. 中心管理者连接外设成功>
- (void)centralManager:(CBCentralManager *)central // 中心管理者
  didConnectPeripheral:(CBPeripheral *)peripheral  {// 外设

    // 连接成功之后,可以进行服务和特征的发现
    // 4.1 获取外设的服务们
    // 4.1.1 设置外设的代理
    self.peripheral.delegate = self;
    
    // 4.1.2 外设发现服务,传nil代表不过滤
    // 这里会触发外设的代理方法 - (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
    [self.peripheral discoverServices:nil];
}

#pragma mark- <3.2. 外设连接失败>
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, line = %d, %@=连接失败", __FUNCTION__, __LINE__, peripheral.name);
}

#pragma mark- <3.3. 丢失连接>
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, line = %d, %@=断开连接", __FUNCTION__, __LINE__, peripheral.name);
}

#pragma mark- 外设代理 CBPeripheralDelegate

#pragma mark- <1. 连接成功后调用此方法,发现外设的 "服务" 后调用的方法>
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
//    NSLog(@"%s, line = %d", __FUNCTION__, __LINE__);
    // 判断没有失败
    if (error) {
//        NSLog(@"%s, line = %d, error = %@", __FUNCTION__, __LINE__, error.localizedDescription);
        return;
    }
    
//    NSLog(@"peripheral.services.count11:%ld",peripheral.services.count);
    
    for (CBService *service in peripheral.services) {
        // 发现服务后,让设备再发现 "服务" 内部的 "特征们" peripheral:didDiscoverCharacteristicsForService:error:
        [peripheral discoverCharacteristics:nil forService:service];
        
//        NSLog(@"service:%@",service);
//        NSLog(@"service->UUID:%@",service.UUID.UUIDString);
    }
}

#pragma mark- <2. 发现外设 服务里的特征 的时候调用的代理方法>
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    /*
    CBCharacteristicProperty Broadcast												= 0x01,
    CBCharacteristicProperty Read		= 0x02,
    CBCharacteristicProperty WriteWithoutResponse									= 0x04,
    CBCharacteristicProperty Write		= 0x08,
    CBCharacteristicProperty Notify		= 0x10,
    CBCharacteristicProperty Indicate												= 0x20,
    CBCharacteristicProperty AuthenticatedSignedWrites								= 0x40,
    CBCharacteristicProperty ExtendedProperties										= 0x80,
    CBCharacteristicProperty NotifyEncryptionRequired NS_ENUM_AVAILABLE(NA, 6_0)		= 0x100,
    CBCharacteristicProperty IndicateEncryptionRequired = 0x200
    */
//    NSLog(@"service:%@",service);
//    NSLog(@"service.characteristics.count11:%ld",service.characteristics.count11);
    
    for (CBCharacteristic *cha in service.characteristics) {
        
        // 获取特征的值 peripheral:didUpdateValueForCharacteristic:error:
        [peripheral readValueForCharacteristic:cha];
        
        // 获取特征对应的 "描述" peripheral:didDiscoverDescriptorsForCharacteristic:error:
        [peripheral discoverDescriptorsForCharacteristic:cha];
//        NSLog(@"character:%@",cha);
    }
}

#pragma mark- <2.1 更新 特征的 "value" 的时候会调用>
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
//    NSLog(@"%s, line = %d", __FUNCTION__, __LINE__);
    
//    NSLog(@"characteristic:%@",characteristic);
    
//    NSLog(@"characteristic->value:%@",characteristic.value);
    
    for (CBDescriptor *descriptor in characteristic.descriptors) {
        // 它会触发  peripheral:didUpdateValueForDescriptor:error:
        [peripheral readValueForDescriptor:descriptor];
    }
}

#pragma mark- <2.2 发现外设的 特征的 "描述" 数组 >
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error {
//    NSLog(@"%s, line = %d", __FUNCTION__, __LINE__);
    
    // 在此处读取描述即可
    for (CBDescriptor *descriptor in characteristic.descriptors) {
        // 它会触发  peripheral:didUpdateValueForDescriptor:error:
        [peripheral readValueForDescriptor:descriptor];
//        NSLog(@"descriptor:%@",descriptor);
    }
}

#pragma mark- <2.2 更新 特征的 "描述" 的值 的时候会调用>
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    //连接后实时调用此方法
    
//    NSLog(@"descriptor.UUID:%@",descriptor.UUID);
//    NSLog(@"descriptor.value:%@",descriptor.value);
//    
//    NSLog(@"characteristic.UUID:%@",descriptor.characteristic.UUID);
//    
//    NSString *characValue=[[NSString alloc]initWithData:descriptor.characteristic.value encoding:NSUTF8StringEncoding];
//    NSLog(@"characValue:%@",characValue);
    
    
    NSString *str=[NSString stringWithFormat:@"%@",descriptor.characteristic.UUID];
    
    
    // 订阅 2A37 读取心率带
    if ([str isEqualToString:@"2A37"]) {
        if (!descriptor.characteristic.isNotifying) {
            [self srx_peripheral:peripheral regNotifyWithCharacteristic:descriptor.characteristic];
        }
    
        NSData *data = descriptor.characteristic.value;
        NSString *value = [NSString stringWithFormat:@"%@",[data description]];
        
        NSInteger amount = [self translateIntoInt:value];
        
//        NSLog(@"%ld",amount);
        
        if (self.count37 < 5) {
            [self.storeArr37 addObject:@(amount)];
        } else if (self.count37 == 5) {
            NSLog(@"37---- %@",self.storeArr37);
        }
        
        self.count37 ++;
        self.heartRate.text = [NSString stringWithFormat:@"%ld",amount];
    }
    
    
     // 查看 FF11 的值改变
    if([str isEqualToString:@"FF11"]) {
        if(!descriptor.characteristic.isNotifying){
            [self srx_peripheral:peripheral regNotifyWithCharacteristic:descriptor.characteristic];
        }
        
//        NSLog(@"FF11-----:%@",descriptor.characteristic);
        
        NSData *data = descriptor.characteristic.value;
        NSString *value = [NSString stringWithFormat:@"%@",[data description]];
        
//         NSString *str = [[NSString alloc] initWithData:descriptor.characteristic.value encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"%@",value);
        
        [self sortToArrays:value];
    }

    // 查看 FF12 的值改变 每秒钟 4 、 5 次
    if([str isEqualToString:@"FF12"]) {
        if(!descriptor.characteristic.isNotifying){
            [self srx_peripheral:peripheral regNotifyWithCharacteristic:descriptor.characteristic];
        }
    
//        NSLog(@"FF12-----:%@",descriptor.characteristic);
//        NSString *str = [[NSString alloc] initWithData:descriptor.characteristic.value encoding:NSUTF8StringEncoding];
        
        NSData *data = descriptor.characteristic.value;
        NSString *value = [NSString stringWithFormat:@"%@",[data description]];
        
//        NSLog(@"FF12 value:%@",value);
        
        [self sortToArraysACC:[data description]];
    }

    // 这里当 “描述的值” 更新的时候,直接调用此方法即可 peripheral:didUpdateValueForDescriptor:error:
    [peripheral readValueForDescriptor:descriptor];
}

- (void)sortToArraysACC:(NSString *)string {
    NSString *clipStr = [string substringWithRange:NSMakeRange(1, string.length - 2)];
    
    NSArray *arr = [clipStr componentsSeparatedByString:@" "];
    //    NSLog(@"arr---:%@",arr);
    
    NSMutableArray *subarr12 = [NSMutableArray array];
    
    for(int i = 0; i < arr.count; i++){
        
        NSInteger count = [self translateIntoInt:arr[i]];
        
        [subarr12 addObject:@(count)];
        
        switch (i) {
            case 0:
                self.acc1.text = [NSString stringWithFormat:@"%ld",count];
                break;
            case 1:
                self.acc2.text = [NSString stringWithFormat:@"%ld",count];
                break;
            case 2:
                self.acc3.text = [NSString stringWithFormat:@"%ld",count];
                break;
            case 3:
                self.acc4.text = [NSString stringWithFormat:@"%ld",count];
                break;
            case 4:
                self.acc5.text = [NSString stringWithFormat:@"%ld",count];
                break;
                
            default:
                break;
        }
        
        
    }
    
    if(self.count12 < 20){
        [self.storeArr12 addObject:subarr12];
    }
    else if(self.count12 == 20){
        NSLog(@"FF12---- %@",self.storeArr12);
    }
    
    self.count12 ++;
}

- (void)sortToArrays:(NSString *)string {
    NSString *clipStr = [string substringWithRange:NSMakeRange(1, string.length - 2)];
    
    NSArray *arr = [clipStr componentsSeparatedByString:@" "];
//    NSLog(@"arr---:%@",arr);
    
    NSMutableArray *substoreArr11 = [NSMutableArray array];
    
    for(int i = 0; i < arr.count; i++){
        
        NSInteger count = [self translateIntoInt:arr[i]];
        
        [substoreArr11 addObject:@(count)];
        switch (i) {
            case 0:
                self.ecg1.text = [NSString stringWithFormat:@"%ld",(long)count];
                break;
                
            case 1:
                self.ecg2.text = [NSString stringWithFormat:@"%ld",(long)count];
                break;
            case 2:
                self.ecg3.text = [NSString stringWithFormat:@"%ld",(long)count];
                break;
            case 3:
                self.ecg4.text = [NSString stringWithFormat:@"%ld",(long)count];
                break;
                
            default:
                break;
        }
        
        
    }
    
    if(self.count11 < 60){
        [self.storeArr11 addObject:substoreArr11];
    }
    else if(self.count11 == 60){
        NSLog(@"FF11----  %@",self.storeArr11);
    }
    
    self.count11 ++;
}



// 将字符串转成int型
- (NSInteger)translateIntoInt:(NSString *)string {
    NSString *clipStr = [string substringWithRange:NSMakeRange(1, string.length - 2)];
//    NSLog(@"clipStr:%@",clipStr);
    
    NSInteger amount = 0;
    
    for (int i = 0; i < clipStr.length; i++) {
        NSString *enumStr = [clipStr substringWithRange:NSMakeRange(i, 1)];
        NSInteger enumNum = [enumStr integerValue];
    
        if(enumNum == 0 && ![enumStr isEqualToString:@"0"])
            enumNum = [self charTranslateIntoInt:enumStr];
        
        double y = clipStr.length -1 - i;
        amount += enumNum * pow(16, y);
    }
    
    return amount;
}

- (NSInteger)charTranslateIntoInt:(NSString *)string {
    NSInteger num;
    if ([string isEqualToString:@"a"]) {
        num = 10;
    }
    else if ([string isEqualToString:@"b"]) {
        num = 11;
    }
    else if ([string isEqualToString:@"c"]) {
        num = 12;
    }
    else if ([string isEqualToString:@"d"]) {
        num = 13;
    }
    else if ([string isEqualToString:@"e"]) {
        num = 14;
    }
    else if ([string isEqualToString:@"f"]) {
        num = 15;
    }

    return num;
}



#pragma mark - 自定义方法
// 一般第三方框架or自定义的方法,可以加前缀与系统自带的方法加以区分.最好还设置一个宏来取消前缀

// 5.外设写数据到特征中

// 需要注意的是特征的属性是否支持写数据
- (void)srx_peripheral:(CBPeripheral *)peripheral didWriteData:(NSData *)data forCharacteristic:(nonnull CBCharacteristic *)characteristic {
    /*
     typedef NS_OPTIONS(NSUInteger, CBCharacteristicProperties) {
     CBCharacteristicPropertyBroadcast												= 0x01,
     CBCharacteristicPropertyRead													= 0x02,
     CBCharacteristicPropertyWriteWithoutResponse									= 0x04,
     CBCharacteristicPropertyWrite													= 0x08,
     CBCharacteristicPropertyNotify													= 0x10,
     CBCharacteristicPropertyIndicate												= 0x20,
     CBCharacteristicPropertyAuthenticatedSignedWrites								= 0x40,
     CBCharacteristicPropertyExtendedProperties										= 0x80,
     CBCharacteristicPropertyNotifyEncryptionRequired NS_ENUM_AVAILABLE(NA, 6_0)		= 0x100,
     CBCharacteristicPropertyIndicateEncryptionRequired NS_ENUM_AVAILABLE(NA, 6_0)	= 0x200
     };
     
     打印出特征的权限(characteristic.properties),可以看到有很多种,这是一个NS_OPTIONS的枚举,可以是多个值
     常见的又read,write,noitfy,indicate.知道这几个基本够用了,前俩是读写权限,后俩都是通知,俩不同的通知方式
     */
    NSLog(@"%s, line = %d, char.pro = %lu", __FUNCTION__, __LINE__, (unsigned long)characteristic.properties);
    // 此时由于枚举属性是NS_OPTIONS,所以一个枚举可能对应多个类型,所以判断不能用 = ,而应该用包含&
    if (characteristic.properties & CBCharacteristicPropertyWrite) {
        // 核心代码在这里
        [peripheral writeValue:data // 写入的数据
             forCharacteristic:characteristic // 写给哪个特征
                          type:CBCharacteristicWriteWithResponse];// 通过此响应记录是否成功写入
    }
}

// 6.通知的订阅和取消订阅
// 实际核心代码是一个方法
// 一般这两个方法要根据产品需求来确定写在何处
- (void)srx_peripheral:(CBPeripheral *)peripheral regNotifyWithCharacteristic:(nonnull CBCharacteristic *)characteristic {
    // 外设为特征订阅通知 数据会进入 peripheral:didUpdateValueForCharacteristic:error:方法
    [peripheral setNotifyValue:YES forCharacteristic:characteristic];
}

- (void)srx_peripheral:(CBPeripheral *)peripheral CancleRegNotifyWithCharacteristic:(nonnull CBCharacteristic *)characteristic {
    // 外设取消订阅通知 数据会进入 peripheral:didUpdateValueForCharacteristic:error:方法
    [peripheral setNotifyValue:NO forCharacteristic:characteristic];
}

// 7.断开连接
- (void)srx_dismissConentedWithPeripheral:(CBPeripheral *)peripheral {
    // 停止扫描
    [self.centralManager stopScan];
    // 断开连接
    [self.centralManager cancelPeripheralConnection:peripheral];
}

@end
