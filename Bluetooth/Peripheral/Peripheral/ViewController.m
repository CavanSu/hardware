//
//  ViewController.m
//  Peripheral
//
//  Created by CavanSu on 17/3/15.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

static NSString *const Service1StrUUID = @"Service1StrUUID";
static NSString *const Service2StrUUID = @"Service2StrUUID";

static NSString *const notiyCharacteristicStrUUID = @"notiyCharacteristicStrUUID";
static NSString *const readwriteCharacteristicStrUUID = @"readwriteCharacteristicStrUUID";
static NSString *const readCharacteristicStrUUID = @"readCharacteristicStrUUID";
static NSString *const LocalNameKey = @"CavanSu";

@interface ViewController () <CBPeripheralManagerDelegate>
@property (nonatomic, strong) CBPeripheralManager *peripheralManager;
@end

@implementation ViewController

- (CBPeripheralManager *)peripheralManager {
    if (!_peripheralManager) {
        _peripheralManager=[[CBPeripheralManager alloc]initWithDelegate:self queue:dispatch_get_main_queue()];
    }
    return _peripheralManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self peripheralManager];
}

#pragma mark- CBPeripheralManagerDelegate

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        //设置外设的 服务和特征
        [self setPeripheralMgr];
    }
    else {
        //模拟器无法打开
        NSLog(@"open blueTooth fail");
    }
}

- (void)setPeripheralMgr {
    //1.设置服务
    CBUUID *serveUUID1=[CBUUID UUIDWithString:Service1StrUUID];
    CBMutableService *service0=[[CBMutableService alloc]initWithType:serveUUID1 primary:YES];
    
//    CBUUID *serveUUID2=[CBUUID UUIDWithString:Service2StrUUID];
//    CBMutableService *service1=[[CBMutableService alloc]initWithType:serveUUID2 primary:YES];
    
    //2.设置特征
    NSString *str1=readCharacteristicStrUUID;
    NSData *data1=[str1 dataUsingEncoding:NSUTF8StringEncoding];
    
    CBMutableCharacteristic *cha0=[[CBMutableCharacteristic alloc]initWithType:[CBUUID UUIDWithString:readCharacteristicStrUUID] properties:CBCharacteristicPropertyRead value:data1 permissions:CBAttributePermissionsReadable];
    
    //3.设置特征的描述
    NSString *str2 = CBUUIDCharacteristicUserDescriptionString;
    NSData *data2 = [str1 dataUsingEncoding:NSUTF8StringEncoding];
    CBMutableDescriptor *descriptor1 = [[CBMutableDescriptor alloc]initWithType:[CBUUID UUIDWithString:CBUUIDCharacteristicUserDescriptionString] value:data2];
    
    //4.将特征的描述放入特征
    cha0.descriptors = @[descriptor1];
    
    //5.将特征放入服务
    service0.characteristics = @[cha0];
   
    //6.将服务放入外设
    [self.peripheralManager addService:service0];
//    [self.peripheralManager addService:service1];
}

@end
